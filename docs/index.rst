.. title:: Docs

.. image:: _images/logo.png
   :target: index.html
   :width: 50%

.. raw:: html

   <br>
   
.. image:: https://img.shields.io/badge/License-MIT-blue.svg
   :target: https://gitlab.com/astroquasar/programs/qscan/-/raw/master/LICENSE
.. image:: https://badge.fury.io/py/qscan.svg
   :target: https://pypi.python.org/pypi/qscan/
.. image:: https://img.shields.io/pypi/dm/qscan
.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.597138.svg
   :target: https://doi.org/10.5281/zenodo.597138
	    
This documentation aims to provide detailed information regarding the use of the QSCAN program. QSCAN stands for quasar scanner and was built to ease the manual identification of absorption line systems in quasar spectra. The program allows the user to scan back and forth any quasar spectrum, manually select the fitting regions for any transition, and create a preliminary fort.13 input file that can be used by the VPFIT software to do least-square Voigt profile fitting. This program was built by `Vincent Dumont <https://vincentdumont.gitlab.io/>`_ and is part of the `AstroQuasar <https://astroquasar.gitlab.io/>`_ group's toolset available in our `GitLab group <https://gitlab.com/astroquasar/programs/qscan>`_.

.. image:: _images/example.jpg
	   
Installation
------------

The package can be easily installed via the ``pip`` Python package manager as follows::

  sudo pip install qscan

Once installed, the software can be executed simply by typing ``qscan`` on the terminal. If the path to a readable spectrum is correct, this will open an interactive matplotlib window displaying all the metal transitions regions (from either the default or user-defined list).

List of commands
----------------

Below is the list of available commands in the program. The list can also be displayed at any time by typing the question mark, ``?``, from the interactive matplotlib window.

.. code-block:: text

   -------------------------------------------------------
       List of Commands
   -------------------------------------------------------
   <space> | scan the spectrum in velocity range
    <down> | decrease column density
      <up> | increase column density
    <left> | decrease Doppler parameter
   <right> | increase Doppler parameter
       [/] | select left/right edge of blend region
       +/- | add/remove Voigt profile
         * | position of atmospheric lines
         = | select given Voigt profile
         . | show deuterium position at -82 km/s
         ? | list of commands
         a | select the anchor transition
         b | specify Doppler parameter
         c | include floating continuum
         d | delete all transitions
         e | select right edge of the fitting region
         f | save fitting regions in fort.13
         g | change displayed velocity dispersion
         h | switch between HI and metal lists
         i | show information for selected regions
         l | identify region for blend searching
         n | specify column density
         p | move to given absorption redshift
         q | leave the program
         r | clear individual fitting region
         s | create PDF of the system
         v | move Voigt profile to selected position
         x | remove blending target region
         w | select left edge of the fitting region
         z | include floating zero
   -------------------------------------------------------

Release Updates
---------------

**v2.5.1**
  The argparse is now used to read the user-input arguments. A few fixes were also made to avoid a warning message due to a bug in Matplotlib, see `this StackOverflow post <https://stackoverflow.com/questions/63723514/userwarning-fixedformatter-should-only-be-used-together-with-fixedlocator>`_ for more information.
**v2.3.1**
  Add overflow hiding features to ensure that data from specific spectral regions don't overflow on other regions.
**v2.2.1**
  Remove print command that was used for debugging but is no longer needed.
**v2.2.0**
  Fixed a bug that prevent the program from selecting a proper transition in the atom.dat when a custom list of metal transition was used.
**v2.1.0**
  Fixed a bug occurring when fort.13 was used as input.

Reporting issues
----------------

If you find any bugs when running this program, please make sure to report them by opening a new issues directly from the Gitlab's repository at the following page:

https://gitlab.com/astroquasar/programs/qscan/-/issues

Open contributions
------------------

I invite anyone who likes and uses the software software to contribute and help me improving its functionalities. This is 

Acknowledgment
--------------

Please acknowledge QSCAN in your publications using the following citation:

.. code-block:: latex

  @software{qscan,
    author       = {Vincent Dumont},
    title        = {QScan: Quasar spectra scanning tool},
    month        = mar,
    year         = 2017,
    publisher    = {Zenodo},
    version      = {v1.0.0},
    doi          = {10.5281/zenodo.597138},
    url          = {https://doi.org/10.5281/zenodo.597138}
  }

License Agreement
-----------------

.. literalinclude:: ../LICENSE
   :language: html   


.. toctree::
   :hidden:

   modules
   tutorial
   rescale
   
