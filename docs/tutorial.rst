Getting Started
===============

In this page, we present a full example that describes all the steps from opening a quasar spectrum to manually select fiting regions of an identified absorption system and save the regions as an input file for follow-up Voigt profile fitting using the VPFIT software.

Read the spectrum
-----------------

In this tutorial, we will be using a spectrum from quasar J042214-384452. We start scanning the spectrum at an absorption redshift of 2 using a custom list of metal transition regions we will be scanning and saved in a local ``linelist.dat`` file where the content is written as follows:

.. code-block:: text

   FeII 2382
   FeII 2600
   FeII 2344
   FeII 2586
   FeII 1608
   FeII 2374
   SiIV 1393
   SiIV 1402
   SiII 1260
   SiII 1193
   SiII 1190
   SiII 1526
   SiII 1304
   SiII 1020
   OI 1302
   OI 988

The program can be run using the following command with the appropriate options:

.. code-block:: text

  qscan J042214-384452.fits --zabs 2 --list linelist.dat

Once the above command is run, the following window below should appear. This shows, for each selected transition, the region between -700 and +700 km/s around the central redshift of 2. The two dotted original lines for each transition correspond to the 0 and +1 level of the flux.
  
.. figure:: _images/snapshot1.png
   :align: center
   :width: 80%

If a spectrum is perfectly normalised in flux, it should not exceed 1 or go below 0. In this example, all the FeII and SiIV region, at that redshift, appear to be decently normalised. However, the SiII transitions clearly have normalisation problems with a couple of extremely noisy transitions and the rest exceeding a flux level of 1.

Identifying systems
-------------------

Using the ``space`` bar from either side of the vertical red line, the user can move either forward or backward in the spectrum. The x position of the cursor at the time of click will determine where the spectrum will be moving in velocity space. You can also moving to a specific redshift in the spectrum by using the keystroke ``p`` and typing in the terminal window any redshift value, for instance in this example 3.086.

.. figure:: _images/snapshot2.png
   :align: center
   :width: 80%
   
Create VPFIT input file
-----------------------

Once you have identify an absorption systems, that is a region where absorption features from different transitions are aligned in velocity space, you can store that system into a fort.13 file which will then be used by the Voigt profile fitting program VPFIT fit the entire system. For a fort.13 file to be created though, one needs to select the transitions that wish to be fitted. This can be done by selecting the edges of the desired fitting region for each of the transitions, which can be performed using the keystroke ``e`` on both right and left edges of the regions.

.. figure:: _images/snapshot3.png
   :align: center
   :width: 80%

Once you have carefully selected all the fitting regions, you can save the results in a fort.13 file by using the ``f`` keystroke. The fort.13 will be saved under a newly created repository that has the quasar name. Below is what the fort.13 looks like in our example. All the selected regions can then be removed using the command ``d`` and typing Y or simply press Enter on the terminal to confirm the deletion.

.. code-block:: text

      *
   J042214-384452.fits    1    6574.39   6577.33   vsig=2.5   ! FeII 1608.45
   J042214-384452.fits    1    9705.41   9709.58   vsig=2.5   ! FeII 2374.46
   J042214-384452.fits    1    5692.94   5700.98   vsig=2.5   ! SiIV 1393.76
   J042214-384452.fits    1    5729.75   5737.73   vsig=2.5   ! SiIV 1402.77
   J042214-384452.fits    1    5148.20   5155.05   vsig=2.5   ! SiII 1260.42
   J042214-384452.fits    1    4873.22   4879.55   vsig=2.5   ! SiII 1193.29
   J042214-384452.fits    1    4861.95   4869.50   vsig=2.5   ! SiII 1190.42
   J042214-384452.fits    1    6235.47   6244.24   vsig=2.5   ! SiII 1526.71
   J042214-384452.fits    1    5326.84   5334.06   vsig=2.5   ! SiII 1304.37
   J042214-384452.fits    1    4171.59   4173.85   vsig=2.5   ! SiII 1020.70
   J042214-384452.fits    1    5322.73   5325.42   vsig=2.5   ! OI 1302.17
     *
      FeII     15.00000     3.0882017aa   10.0000la     0.00E+00q       0.00   0.00E+00  0 !    1
      SiIV     15.00000     3.0882017AA   10.0000LA     0.00E+00Q       0.00   0.00E+00  0 !    1
      SiII     15.00000     3.0882017AA   10.0000LA     0.00E+00Q       0.00   0.00E+00  0 !    1
      OI       15.00000     3.0882017AA   10.0000LA     0.00E+00Q       0.00   0.00E+00  0 !    1
   
