Code structure
==============

Below are all the functions present in the proram. The source code for each module can be accessed by click on the module name and the ``[source]`` link on the module's webpage. This program was written while exploring atypical ways of coding in Python. Therefore, as you will notice, most of the function do not have input arguments, this is because most global variables are defined within the :mod:`qscan.variables` function.

.. currentmodule:: qscan

.. autosummary::
   :toctree: generated/

   atominfo
   gothrough
   info
   makeatomlist
   plotreg
   readfort13
   readspec
   savefort13
   scan
   specplot
   switch
   transplot
   voigtcomp
   voigtking
   voigtmodel
