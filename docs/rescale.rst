Velocity Range
==============

Changing the displayed velocity range is quite straightforward and can be done as follows:

1. Press the "g" key from the interactive matplotlib window.
2. The above action will prompt a message on the terminal asking you to specify the new velocity dispersion dv (in km/s). In order to facilitate the visualization, vertical lines have been displayed on the interactive window at each displayed x-tickmark.
3. After writing down the new range and pressing enter, the interactive window will be refreshed and all the transitions should now be displayed on the newly defined velocity range.

The slideshow below gives a preview of how things should look like during each of the steps above described (use the green arrows on the right/left of the images to look through the slideshow):

.. raw:: html

   <style>
     * {box-sizing: border-box}
     .mySlides1, .mySlides2 {display: none}
     img {vertical-align: middle;}
   
     /* Slideshow container */
     .slideshow-container {
       max-width: 1000px;
       position: relative;
       margin: auto;
     }
   
     /* Next & previous buttons */
     .prev, .next {
       cursor: pointer;
       position: absolute;
       top: 50%;
       width: auto;
       padding: 1px;
       margin-top: -22px;
       color: white;
       font-weight: bold;
       font-size: 50px;
       transition: 0.6s ease;
       border-radius: 0 3px 3px 0;
       user-select: none;
     }
   
     /* Position the "next button" to the right */
     .next {
       right: 0;
       border-radius: 3px 0 0 3px;
     }
   
     /* On hover, add a grey background color */
     .prev:hover, .next:hover {
       background-color: #f1f1f1;
       color: black;
     }
   </style>
   
	 
   <div class="slideshow-container">
     <div class="mySlides1">
       <b>1 / 4</b>
       <img src="_static/rescale1.png" style="width:100%">
     </div>
   
     <div class="mySlides1">
       <b>2 / 4</b>
       <img src="_static/rescale2.png" style="width:100%">
     </div>
   
     <div class="mySlides1">
       <b>3 / 4</b>
       <img src="_static/rescale3.png" style="width:100%">
     </div>
   
     <div class="mySlides1">
       <b>4 / 4</b>
       <img src="_static/rescale4.png" style="width:100%">
     </div>
   
     <a class="prev" onclick="plusSlides(-1, 0)">&#10094;</a>
     <a class="next" onclick="plusSlides(1, 0)">&#10095;</a>
   </div>
   
   <script>
   var slideIndex = [1,1];
   var slideId = ["mySlides1", "mySlides2"]
   showSlides(1, 0);
   showSlides(1, 1);
   
   function plusSlides(n, no) {
     showSlides(slideIndex[no] += n, no);
   }
   
   function showSlides(n, no) {
     var i;
     var x = document.getElementsByClassName(slideId[no]);
     if (n > x.length) {slideIndex[no] = 1}    
     if (n < 1) {slideIndex[no] = x.length}
     for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";  
     }
     x[slideIndex[no]-1].style.display = "block";  
   }
   </script>
   
